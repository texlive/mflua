#!/bin/sh

B=build
cwd=$(pwd)
configure=../source/configure
reautoconf=../source/reautoconf
MAKEONLY=false
REAUTOCONF=false
DEBUG=false

STRIPBIN=true
STRIP=strip

JOBS_IF_PARALLEL=4
MAX_LOAD_IF_PARALLEL=4

TEXLIVEOPT=
CONFHOST=
CONFBUILD=
MINGWCROSS64=false
MINGWCROSS=false
EXE=""

HELP=false
CHECK=true

MAKE=make
if make -v 2>&1| grep "GNU Make" >/dev/null
then 
  echo "Your make is a GNU-make; I will use that"
elif gmake -v >/dev/null 2>&1
then
  MAKE=gmake;
  export MAKE;
  echo "You have a GNU-make installed as gmake; I will use that"
else
  echo "I can't find a GNU-make; I'll try to use make and hope that works." 
  echo "If it doesn't, please install GNU-make."
fi


until [ -z "$1" ]; do
  case "$1" in
    --nocheck    ) CHECK=false        ;;
    --make       ) MAKEONLY=true      ;;
    --reautoconf ) REAUTOCONF=true    ;;
    --debug      ) DEBUG=true         ;;
    --mingw      ) MINGWCROSS=true    ;;
    --mingw32    ) MINGWCROSS=true    ;;
    --mingw64    ) MINGWCROSS64=true  ;;
    --parallel   ) MAKE="$MAKE -j $JOBS_IF_PARALLEL -l $MAX_LOAD_IF_PARALLEL" ;;
    --host=*     ) CONFHOST="$1"      ;;
    --build=*    ) CONFBUILD="$1"     ;;
    --tlopt=*    ) TEXLIVEOPT=`echo $1 | sed 's/--tlopt=\(.*\)/\1/' `      ;;
    --help       ) HELP=true          ;;
    *            ) echo "ERROR: invalid build.sh parameter: $1. Use --help for the list of valid parameters."; exit 1       ;;

  esac
  shift
done

if [ "$DEBUG" = "true" ]
then
 STRIPBIN=false
fi

if [ "$HELP" = "true" ]
then
 echo
 echo "--debug:      compile with debug symbols"
 echo "--make:       only make, no configure"
 echo "--mingw:      cross-compile for Windows 32bit"
 echo "--mingw64:    cross-compile for Windows 64bit"
 echo "--parallel:   parallel make with jobs=$JOBS_IF_PARALLEL and load=$MAX_LOAD_IF_PARALLEL"
 echo "--reautoconf: run reautoconf"
 echo "--host=*      host target"      
 echo "--build=*     build target"     
 echo "--tlopt=*     TeXLive extra option"     
 echo "--help:       this help"
 exit 0
fi


if [ "$MINGWCROSS64" = "true" ]
then
  B=build-windows64
  MINGWCROSS=false
  OLDPATH=$PATH
  PATH=/usr/mingw32/bin:$PATH
  PATH=`pwd`/extrabin/mingw:$PATH
  CFLAGS="-mtune=nocona -g -O3 -fno-lto -fno-use-linker-plugin $CFLAGS"
  CXXFLAGS="-mtune=nocona -g -O3 -fno-lto -fno-use-linker-plugin $CXXFLAGS"
  : ${CONFHOST:=--host=x86_64-w64-mingw32}
  : ${CONFBUILD:=--build=x86_64-unknown-linux-gnu}
  RANLIB="${CONFHOST#--host=}-ranlib"
  STRIP="${CONFHOST#--host=}-strip"
  LDFLAGS="${LDFLAGS} -fno-lto -fno-use-linker-plugin -static-libgcc -static-libstdc++"
  export CFLAGS CXXFLAGS LDFLAGS
  EXE=".exe"
fi

if [ "$MINGWCROSS" = "true" ]
then
  B=build-windows
  MINGWCROSS64=false
  OLDPATH=$PATH
  PATH=/usr/mingw32/bin:$PATH
  PATH=`pwd`/extrabin/mingw:$PATH
  CFLAGS="-mtune=nocona -g -O3 $CFLAGS"
  CXXFLAGS="-mtune=nocona -g -O3 $CXXFLAGS"
  : ${CONFHOST:=--host=i686-w64-mingw32}
  : ${CONFBUILD:=--build=x86_64-unknown-linux-gnu}
  RANLIB="${CONFHOST#--host=}-ranlib"
  STRIP="${CONFHOST#--host=}-strip"
  LDFLAGS="-Wl,--large-address-aware -Wl,--stack,2621440 $CFLAGS"
  export CFLAGS CXXFLAGS LDFLAGS BUILDCXX BUILDCC
  EXE=".exe"
fi

WARNINGFLAGS=--enable-compiler-warnings=all

if [ $DEBUG = "true" ]
then
 export CFLAGS="$CFLAGS -g -O0 -g3"  
 export CXXFLAGS="$CXXFLAGS -g -O0 -g3"
fi


if [ $MAKEONLY = "false" ]
then
 rm -fr $B
 mkdir $B
fi

cd $B

if [ $MAKEONLY = "false" ]
then
 if [ $REAUTOCONF = "true" ]
 then
   cd ${cwd}/source
   ./reautoconf
 fi
 cd ${cwd}
 cd $B ; 
 $configure --disable-all-pkgs  --enable-web2c $TEXLIVEOPT $CONFHOST $CONFBUILD  $WARNINGFLAGS\
  --disable-etex          \
  --disable-ptex          \
  --disable-eptex         \
  --disable-uptex         \
  --disable-euptex        \
  --disable-aleph         \
  --disable-pdftex        \
  --disable-luatex        \
  --disable-luahbtex      \
  --disable-luajittex     \
  --disable-luajithbtex   \
  --disable-mp            \
  --disable-pmp           \
  --disable-upmp          \
  --disable-xetex         \
  --disable-tex-synctex     \
  --disable-synctex       \
  --without-system-libpng \
  --without-system-gd    \
  --without-system-freetype2 \
  --without-system-poppler \
  --without-system-teckit \
  --without-system-zlib  \
  --without-system-ptexenc \
  --without-system-kpathsea \
  --without-mf-x-toolkit --without-x \
  --without-mflua-x-toolkit \
  --enable-cxx-runtime-hack \
  --disable-shared    \
  --disable-largefile \
  --disable-ipc  \
  --enable-dump-share
fi 
##exit
#make  V=1 -j16 -l16 all
$MAKE all
if [ $CHECK = "true" ]
then
 $MAKE check 
fi
#make install



cd ${cwd}
if [ $STRIPBIN = "true" ]
then
 $STRIP "$B/texk/web2c/mflua$EXE"
 #$STRIP "$B/texk/web2c/mflua-nowin$EXE"
 $STRIP "$B/texk/web2c/mfluajit$EXE"

fi

ls -al "$B/texk/web2c/mflua$EXE"
#ls -al "$B/texk/web2c/mflua-nowin$EXE"
ls -al "$B/texk/web2c/mfluajit$EXE"
#ls -al "$B/texk/web2c/mfluajit-nowin$EXE"
